package restAssured;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;


public class RestBasic1 {
	
	String nameCountry="Sydney";
	
	@Test
	public void sampleTest() {
		
		RestAssured.baseURI="https://maps.googleapis.com";
		given().log().all().
		param("location", "-33.8670522,151.1957362").
		param("radius", "1500").
		param("key", "AIzaSyAoKHeBznOZ6Lb2EZJzU4_6r2E1mAEWQhE").
		when().log().all().
		get("/maps/api/place/nearbysearch/json").
		then().assertThat().statusCode(200).contentType(ContentType.JSON).and().
		body("results[0].name",equalTo(nameCountry)).and().
		header("Server", "scaffolding on HTTPServer2");
		
	}
	
	
	@Test
	public void samplePostTest() {
		/*http://dummy.restapiexample.com/*/		
		
		String b= "{\"name\":\"test14785\",\"salary\":\"1234\",\"age\":\"24\"}";
		
		RestAssured.baseURI="http://dummy.restapiexample.com";
		//Create Request 
		Response res =given().log().all().	
		body(b).
		log().all().when().post("/api/v1/create").
		then().log().all().assertThat().statusCode(200).
		extract().response();
		
		//Grab the request which is created
		String responseString = res.asString();
		System.out.println("This is reponse converted into string"+responseString);
		JsonPath js= new JsonPath(responseString);
		String jsonRes=js.get("id");
		System.out.println("This is respone is converted into JSON :"+jsonRes);
		
		//Delete the request which is grab earlier 
		given().
		log().all().when().delete("/api/v1/delete/"+jsonRes+"").
		then().log().all().assertThat().statusCode(200);
		}

}
