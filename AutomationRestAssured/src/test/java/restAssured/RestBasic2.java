package restAssured;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import files.Resources;
import files.ReusableMethods;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class RestBasic2 {
	
	Properties prop=new Properties();
	
	@BeforeTest
	public void getData() throws IOException
	{
		
		FileInputStream fis=new FileInputStream("C:\\Users\\Karan Parmar\\git\\AutomationRestAssured\\src\\test\\java\\files\\env.properties");
		prop.load(fis);
	}

String nameCountry="Sydney";
	
	@Test
	public void sampleTest() {
		
		RestAssured.baseURI=prop.getProperty("GoogleHost");
		Response res = given().log().all().
		param("location", "-33.8670522,151.1957362").
		param("radius", "1500").
		param("key",prop.getProperty("Key")).
		when().log().all().
		get(Resources.getData()).
		then().assertThat().statusCode(200).contentType(ContentType.JSON).and().
		body("results[0].name",equalTo(nameCountry)).and().
		header("Server", "scaffolding on HTTPServer2").
		extract().response();
		JsonPath js =ReusableMethods.rawToJson(res);
		int count= js.get("results.size()");
		System.out.println(count);
		for (int i=0;i<=count;i++) 
		{
			System.out.println(js.get("results["+i+"].name"));
		
		}
		
		
	}
	
	
	@Test
	public void samplePostTest() {
		/*http://dummy.restapiexample.com/*/		
		
		String b= "{\"name\":\"test14785\",\"salary\":\"1234\",\"age\":\"24\"}";
		
		RestAssured.baseURI="http://dummy.restapiexample.com";
		//Create Request 
		Response res =given().log().all().	
		body(b).
		log().all().when().post("/api/v1/create").
		then().log().all().assertThat().statusCode(200).
		extract().response();
		
		//Grab the request which is created
		JsonPath js =ReusableMethods.rawToJson(res);
		System.out.println(js);
		String jsonRes=js.get("id");
		System.out.println("This is respone is converted into JSON :"+jsonRes);
		
		//Delete the request which is grab earlier 
		given().
		log().all().when().delete("/api/v1/delete/"+jsonRes+"").
		then().log().all().assertThat().statusCode(200);
		}

}
