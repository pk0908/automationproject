package files;

import io.restassured.path.json.JsonPath;
import io.restassured.path.xml.XmlPath;
import io.restassured.response.Response;

public class ReusableMethods {

	public static XmlPath rawToXml(Response r) {
		String responseString = r.asString();
		XmlPath x= new XmlPath(responseString);
		return x;
	}
	
	public static JsonPath rawToJson(Response r) {
		String responseString = r.asString();
		JsonPath x= new JsonPath(responseString);
		return x;
	}
}
